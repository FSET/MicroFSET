# MicroFSET

This repository contains two board designs oriented towards first-semester
students that are pursuing a degree program offered by the Faculty of
Electrical Engineering and Information Technology.

## rev0a

This project is the initial version of a simple microcontroller board for our
first semester students. It was created in 2019.

This PCB design contains an integrated USB connector, eight LEDs and switches,
two potentiometers and a buzzer.

### Credits

- Erik Wichmann

## rev2

Revised version with an ATmega32U4 processor and an additional photosensor.
This version was created in 2020, however, the boards themselves were
unfortunately never produced.

Designed using the free- and open-source [KiCAD](https://www.kicad.org/).

### Credits

- Sophie Schüttpelz

Additional support:
- Erik Wichmann
- Karl Sturm

## License

The contents of this repository are licensed under the terms of
[CERN Open Hardware License Version 2 - Permissive](LICENSE).

### Disclaimer

All trademarks, logos and brand names remain the property of their respective
owners and are therefore not distributed under the terms of the aforementioned
license. If you want to distribute a derivative copy of this work while using
restricted logos and trademarks, you have to ask for permission first.

Additionally, none of the material in this repository was produced with
publication in mind. Build artifacts, hardcoded paths or lacking version
control are to be expected.
